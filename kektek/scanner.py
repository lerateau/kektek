import os
import re
import json
import logging

import srt

from multiprocessing import Pool

logger = logging.getLogger(__name__)
       
def clean_up(text):
    return (
        text.replace("!", "").replace(".", "").replace(",", "").replace("?", "").lower()
    )


def parse_srt(srt_file, keywords):
    matches = []
    for sentence in srt.parse(srt_file):
        text = sentence.content
        matches.extend(
            {
                "file": srt_file.name,
                "sentence": text,
                "keyword": keyword,
                "start": str(sentence.start),
                "end": str(sentence.end),
            }
            for keyword in keywords
            if re.search(rf"(^|\s){keyword}($|\s)", clean_up(text))
        )
    return matches


def process_file(file_tuple):
    srt_file = file_tuple[0]
    keywords = file_tuple[1]
    output_path = file_tuple[2]
    
    show_name, episode_id, *_ = srt_file.split("-", 2)
    show_name = show_name.rpartition("/")[2]
    output_filename = show_name[:-1] + episode_id + "matches.jsonl"
    output_filename = f"{output_path}{os.path.sep}{output_filename}"
    
    logging.info("Processing file %r", srt_file)
    with open(srt_file) as handle:
        try:
            detected_list = parse_srt(handle, keywords)
        except (UnicodeError, srt.SRTParseError):
            logger.error("Failed to parse srt file due to unicode error")
            return  
        
    output_dir = output_filename.rsplit(os.path.sep, 1)[0]
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    with open(output_filename, "w+") as output_file:
        for detected in detected_list:
            line = json.dumps(detected)
            output_file.write(line + "\n")
            
def get_file_tuples_to_process(srt_files, keywords, output_path):
    files_to_process = []
    for element in srt_files:
        for root, dirs, files in os.walk(element):
            for file in files:                
                files_to_process.append((os.path.join(root, file), keywords, output_path))
    return files_to_process

def run(srt_files, keywords_file, output_path):
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    keywords = {line.lower().rstrip() for line in keywords_file}
    
    thread_pool = Pool() # By default, uses your number of CPUs
    thread_pool.map(process_file, get_file_tuples_to_process(srt_files, keywords, output_path))        
    
